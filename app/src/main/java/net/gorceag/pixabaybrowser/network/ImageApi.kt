package net.gorceag.pixabaybrowser.network

import io.reactivex.Observable
import net.gorceag.pixabaybrowser.model.Image
import net.gorceag.pixabaybrowser.model.ImageFeed
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author Timur Gorceag
 */

interface ImageApi {
    @GET("/api/?key=10753647-a9b409d6f23bfac981644c8e4&image_type=photo&pretty=true")
    fun getImages(@Query("q") request: String): Observable<ImageFeed>
}