package net.gorceag.pixabaybrowser.utils

/**
 * @author Timur Gorceag
 */

/** The base URL of the API */
const val BASE_URL: String = "https://pixabay.com"