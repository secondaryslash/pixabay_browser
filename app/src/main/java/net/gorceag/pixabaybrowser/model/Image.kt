package net.gorceag.pixabaybrowser.model

/**
 * @author Timur Gorceag
 */

data class Image(
    val largeImageURL: String,
    val webformatHeight: String,
    val webformatWidth: Int,
    val likes: Int,
    val imageWidth: Int,
    val id: Int,
    val user_id: Int,
    val views: Int,
    val comments: Int,
    val pageURL: String,
    val imageHeight: Int,
    val webformatUrl: String,
    val type: String,
    val previewHeight: Int,
    val tags: String,
    val downloads: Int,
    val user: String,
    val favorites: Int,
    val imageSize: Int,
    val previewWidth: Int,
    val userImageURL: String,
    val previewURL: String
)