package net.gorceag.pixabaybrowser.model

/**
 * @author Timur Gorceag
 */
data class ImageFeed(
    val totalHits: Int,
    val hits: List<Image>
)