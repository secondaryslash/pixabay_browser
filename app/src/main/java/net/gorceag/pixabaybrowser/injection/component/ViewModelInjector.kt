package net.gorceag.pixabaybrowser.injection.component

import dagger.Component
import net.gorceag.pixabaybrowser.injection.module.NetworkModule
import net.gorceag.pixabaybrowser.ui.imagelist.ImageListViewModel
import javax.inject.Singleton

/**
 * @author Timur Gorceag
 */
/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified ImageListViewModel.
     * @param imageListViewModel ImageListViewModel in which to inject the dependencies
     */
    fun inject(imageListViewModel: ImageListViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}