package net.gorceag.pixabaybrowser.injection.module

import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.schedulers.Schedulers
import net.gorceag.pixabaybrowser.network.ImageApi
import net.gorceag.pixabaybrowser.utils.BASE_URL
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 */
/**
 * Module which provides all required dependencies about network
 * @author Timur Gorceag
 */

@Module
@Suppress("unused")
object NetworkModule {
    /**
     * Provides the Image service implementation.
     * @param retrofit the Retrofit object used to instantiate the service
     * @return the Image service implementation.
     */
    @Provides
    @Reusable
    @JvmStatic
    internal fun provideImageApi(retrofit: Retrofit): ImageApi {
        return retrofit.create(ImageApi::class.java)
    }

    /**
     * Provides the Retrofit object.
     * @return the Retrofit object
     */
    @Provides
    @Reusable
    @JvmStatic
    internal fun provideRetrofitInterface(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }
}