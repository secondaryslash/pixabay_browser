package net.gorceag.pixabaybrowser.ui.imagelist

import android.arch.lifecycle.MutableLiveData
import net.gorceag.pixabaybrowser.main.BaseViewModel
import net.gorceag.pixabaybrowser.model.Image

/**
 * @author Timur Gorceag
 */
class ImageViewModel: BaseViewModel() {
    private val imageWebformatUrl = MutableLiveData<String>()
    private val imageType = MutableLiveData<String>()
    private val previewUrl = MutableLiveData<String>()

    fun bind(image: Image){
        imageWebformatUrl.value = image.largeImageURL
        imageType.value = image.type
        previewUrl.value = image.previewURL
    }

    fun getImageWebFormatUrl():MutableLiveData<String>{
        return imageWebformatUrl
    }

    fun getImageType():MutableLiveData<String>{
        return imageType
    }

    fun getPreviewUrl():MutableLiveData<String>{
        return previewUrl
    }
}