package net.gorceag.pixabaybrowser.ui.imagelist

import android.databinding.DataBindingUtil
import android.databinding.Observable
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import io.reactivex.subjects.PublishSubject
import net.gorceag.pixabaybrowser.R
import net.gorceag.pixabaybrowser.databinding.ItemImageBinding
import net.gorceag.pixabaybrowser.model.Image
import net.gorceag.pixabaybrowser.model.ImageFeed

/**
 * @author Timur Gorceag
 */
class ImageListAdapter: RecyclerView.Adapter<ImageListItemViewHolder>() {
    private lateinit var imageList:List<Image>

    val clickSubject = PublishSubject.create<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageListItemViewHolder {
        val binding: ItemImageBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_image, parent, false)
        return ImageListItemViewHolder(clickSubject, binding)
    }

    override fun onBindViewHolder(holder: ImageListItemViewHolder, position: Int) {
        holder.bind(imageList[position])
    }

    override fun getItemCount(): Int {
        return if(::imageList.isInitialized) imageList.size else 0
    }

    fun updateImageList(imageFeed: ImageFeed){
        this.imageList = imageFeed.hits
        notifyDataSetChanged()
    }


}