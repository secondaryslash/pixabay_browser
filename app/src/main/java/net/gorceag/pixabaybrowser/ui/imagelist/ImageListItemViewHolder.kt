package net.gorceag.pixabaybrowser.ui.imagelist

import android.support.v7.widget.RecyclerView
import io.reactivex.subjects.PublishSubject
import net.gorceag.pixabaybrowser.databinding.ItemImageBinding
import net.gorceag.pixabaybrowser.model.Image

/**
 * @author Timur Gorceag
 */
class ImageListItemViewHolder(val clickSubject: PublishSubject<String>, private val binding: ItemImageBinding) :
    RecyclerView.ViewHolder(binding.root) {
    private val viewModel = ImageViewModel()
    fun bind(image: Image) {
        viewModel.bind(image)
        binding.viewModel = viewModel
        binding.container.setOnClickListener {
            viewModel.getImageWebFormatUrl().value?.let { clickSubject.onNext(it) }
        }
    }
}