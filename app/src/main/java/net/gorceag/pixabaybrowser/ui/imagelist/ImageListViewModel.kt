package net.gorceag.pixabaybrowser.ui.imagelist

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import net.gorceag.pixabaybrowser.R
import net.gorceag.pixabaybrowser.main.BaseViewModel
import net.gorceag.pixabaybrowser.model.Image
import net.gorceag.pixabaybrowser.model.ImageFeed
import net.gorceag.pixabaybrowser.network.ImageApi
import javax.inject.Inject

/**
 * @author Timur Gorceag
 */
class ImageListViewModel : BaseViewModel() {
    @Inject
    lateinit var imageApi: ImageApi

    private lateinit var subscription: Disposable
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()

    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadImages("home") }

    val imageListAdapter: ImageListAdapter = ImageListAdapter()

    init {
        imageListAdapter.clickSubject
                .subscribe { message ->
                    Log.e("LOGGING", "Clicked on $message")
                }
        loadImages("home")

//        searchQuery.observe(parentActivity, Observer { value -> view.visibility = value ?: View.VISIBLE })
    }

    fun loadImages(queryString: String) {
        subscription = imageApi.getImages(queryString)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrieveImageListStart() }
            .doOnTerminate { onRetrieveImageListFinish() }
            .subscribe(
                { result -> onRetrieveImageListSuccess(result) },
                { result -> onRetrieveImageListError(result) }
            )
    }

    private fun onRetrieveImageListStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrieveImageListFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrieveImageListSuccess(imageFeed: ImageFeed) {
        Log.e("LOGGING", "WE HAVE A SUCCESS")
        imageListAdapter.updateImageList(imageFeed)
    }

    private fun onRetrieveImageListError(t: Throwable) {
        Log.e("LOGGING", t.message)
        errorMessage.value = R.string.image_error
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}