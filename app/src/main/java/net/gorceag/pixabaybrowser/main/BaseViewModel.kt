package net.gorceag.pixabaybrowser.main

import android.arch.lifecycle.ViewModel
import net.gorceag.pixabaybrowser.injection.component.DaggerViewModelInjector
import net.gorceag.pixabaybrowser.injection.component.ViewModelInjector
import net.gorceag.pixabaybrowser.injection.module.NetworkModule
import net.gorceag.pixabaybrowser.ui.imagelist.ImageListViewModel

/**
 * @author Timur Gorceag
 */

abstract class BaseViewModel : ViewModel() {
    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is ImageListViewModel -> injector.inject(this)
        }
    }
}